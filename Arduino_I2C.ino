#include<Wire.h>
const int adress = 0x68, acc_range = 2, gy_range = 250; 
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

void setup(){
  Wire.begin(); //inicjalizacja I2C, dolaczenie arduino jako urzadzenie nadrzedne
  Wire.beginTransmission(adress); // rozpoczecie trasmisji danych do urzadzenia podrzednego(czyli acc)
  Wire.write(0x6B); //rejestr odpowiedzialny za wybranie zrodla oscylatora
  Wire.write(0);    // wybranie wewnetrznego oscylatora 8Mhz
  Wire.endTransmission(true); // zakonczenie transmisji sygnalem STOP
  
  Wire.beginTransmission(adress);
  Wire.write(0x1B);
  Wire.write(0<<3); // wybor zakresu dla zyroskopu
  Wire.endTransmission(true); 
  
  Wire.beginTransmission(adress);
  Wire.write(0x1C);
  Wire.write(0<<3); // wybor zakresu dla akcelerometru
  Wire.endTransmission(true);
   
  Serial.begin(9600); 
}
void loop(){
  // jesli nie am sygnalu STOP to linia jest zarezerwowana (w zawieszeniu) i inne urzadzenie nie moze jej przejac
  Wire.beginTransmission(adress); 
  Wire.write(0x3B);  
  Wire.endTransmission(false);
  // zadanie wyslania 14 bajtow danych przez acc oraz brak wyslanai sygnalu STOP, w ten sposob linia dancyh(magistrala) jest blokowana
  Wire.requestFrom(adress,14,true); 
  
  AcX = Wire.read()<<8|Wire.read();  //zwraca pierwszy 2 bajty z bufora odebranch danych  
  AcY = Wire.read()<<8|Wire.read();  // najpierw [15:8] i [7:0] dlatego jest przesuniecie
  AcZ = Wire.read()<<8|Wire.read();
  Wire.read()<<8|Wire.read();  // pominiecie dla temperatury
  GyX = Wire.read()<<8|Wire.read();  
  GyY = Wire.read()<<8|Wire.read();  
  GyZ = Wire.read()<<8|Wire.read();  
  
  Serial.print("Accelerometer: ");
  Serial.print("X = " + (String)real_value(AcX, acc_range) + " g");
  Serial.print(" | Y = "+(String)real_value(AcY, acc_range) + " g");
  Serial.println(" | Z = "+(String)real_value(AcZ, acc_range) + " g"); 
  
  Serial.print("Gyroscope: ");
  Serial.print("X = "+(String)real_value(GyX, gy_range) + " o/s");
  Serial.print(" | Y = "+(String)real_value(GyY, gy_range) + " o/s");
  Serial.println(" | Z = "+(String)real_value(GyZ, gy_range) + " o/s");
  Serial.println(" ");
  delay(333);
}

float real_value(int16_t value, int range){
  return value / pow(2, 15) * range;
}