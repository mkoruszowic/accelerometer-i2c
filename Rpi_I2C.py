import smbus  # import SMBus module of I2C
from time import sleep

# Some MPU6050 Registers and their Address
PWR_MGMT_1 = 0x6B  # Register responsible for selecting the oscillator source
SMPLRT_DIV = 0x19
CONFIG = 0x1A
GYRO_CONFIG = 0x1B  # Range selection for gyroscope
ACCEL_CONFIG = 0x1C  # Range selection for accelerometer
INT_ENABLE = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H = 0x43
GYRO_YOUT_H = 0x45
GYRO_ZOUT_H = 0x47


def MPU_Init():
    # Write to sample rate register - The Sample Rate is determined by dividing the
    # gyroscope output rate by this value.
    # bus.write_byte_data(Device_Address, SMPLRT_DIV, 7)

    # Write to power management register
    bus.write_byte_data(Device_Address, PWR_MGMT_1, 0)  # wczesniej 1

    # Write to Configuration register
    # bus.write_byte_data(Device_Address, CONFIG, 0)

    # Write to Gyro configuration register
    bus.write_byte_data(Device_Address, GYRO_CONFIG, 0 << 3)
    
    # Write to Accel configuration register
    bus.write_byte_data(Device_Address, ACCEL_CONFIG, 0 << 3)

    # Write to interrupt enable register- When set to 1, this bit enables the Data Ready interrupt, which occurs each
    # time a write operation to all of the sensor registers has been completed.
    # bus.write_byte_data(Device_Address, INT_ENABLE, 1)


def read_raw_data(addr):
    # Accelerometer and Gyroscope value are 16-bit
    high = bus.read_byte_data(Device_Address, addr)
    low = bus.read_byte_data(Device_Address, addr + 1)

    # Concatenate higher and lower value
    value = ((high << 8) | low)

    # to get signed value from mpu6050
    if (value > 32768):
        value = value - 65536
    return value


def real_value(value, range):
    return value / 2 ** 15 * range


bus = smbus.SMBus(1)  # or bus = smbus.SMBus(0) for older version boards
Device_Address = 0x68  # MPU6050 device address
gyro_range = 250
acc_range = 2
t = 0
MPU_Init()

print(" Reading Data of Gyroscope and Accelerometer")

while True:
    # Read Accelerometer raw value
    acc_x = read_raw_data(ACCEL_XOUT_H)
    acc_y = read_raw_data(ACCEL_YOUT_H)
    acc_z = read_raw_data(ACCEL_ZOUT_H)

    # Read Gyroscope raw value
    gyro_x = read_raw_data(GYRO_XOUT_H)
    gyro_y = read_raw_data(GYRO_YOUT_H)
    gyro_z = read_raw_data(GYRO_ZOUT_H)

    # Full scale range +/- 250 degree/C as per sensitivity scale factor

    Ax = real_value(acc_x, acc_range)
    Ay = real_value(acc_y, acc_range)
    Az = real_value(acc_z, acc_range)
    

    Gx = real_value(gyro_x, gyro_range)
    Gy = real_value(gyro_y, gyro_range)
    Gz = real_value(gyro_z, gyro_range)

    t += 0.2

    with open('data.txt', 'a') as file:
        file.writelines('{} {} {} {}\n'.format(Ax, Ay, Az, t))

    print("Ax = {:.2f}g | Ay = {:.2f}g | Az = {:.2f}g".format(Ax, Ay, Az))
    # print('Gx = {:.2f}\xb0 | Gy = {:.2f}\xb0| Gz = {:.2f}\xb0'.format(Gx, Gy, Gz))
    sleep(0.2)

