import matplotlib.pyplot as plt

Ax, Ay, Az, time = [], [], [], []
name = 'data'
with open(name + '.txt', 'r') as file:
    for line in file:
        array = line.split()
        Ax.append(float(array[0]))
        Ay.append(float(array[1]))
        Az.append(float(array[2]))
        time.append(float(array[3]))

plt.plot(time, Ax, '.', label='Ax')
plt.plot(time, Ay, '.', label='Ay')
plt.plot(time, Az, '.', label='Az')
plt.legend()
plt.xlabel('Time, s')
plt.ylabel('Acceleration, g')
plt.savefig(name + '.png')
plt.show()

